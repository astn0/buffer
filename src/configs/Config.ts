import IConfig from "./IConfig"

const Config: IConfig = {
  port: '8080',
  bufferMax: '0',
  timeToWait: '0'
} 
export default Config