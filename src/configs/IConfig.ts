export default interface IConfig {
  port: string,
  bufferMax: string,
  timeToWait: string
} 