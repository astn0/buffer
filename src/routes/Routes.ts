import Router from 'express-promise-router'
import DataController from '../controllers/DataController'
const router = Router()

router.route('/v1/data/').get(DataController.getData)
router.route('/v1/data/').post(DataController.addDataToBuffer)

export default router