import ClickHouseDataDomain from "../../domains/ClickHouseDataDomain";

export default class LoadInterval {

  private _clickHouseDataDomain: ClickHouseDataDomain

  constructor (bufferMax: number) {
    this._clickHouseDataDomain = new ClickHouseDataDomain(bufferMax)
  }

  public async pushFromBufferToCH () {
    this._clickHouseDataDomain.pushFromBufferToCH()
  }

}