import Redis from 'ioredis'
export default class RedisSingleton {
  private static _instance: RedisSingleton
  private _redis: any // todo: add type for redis

  private constructor (redisInstance: any) {
    this._redis = redisInstance
  }

  public get redis () {
    return this._redis
  }

  public static async getInstance() {
    if (!RedisSingleton._instance) {
      console.log('создаем синглтон')
      const redisInstance = new Redis()
      console.log('Подключили redis синглтон')
      RedisSingleton._instance = new RedisSingleton(redisInstance) 
    }
    return RedisSingleton._instance 
  }
}