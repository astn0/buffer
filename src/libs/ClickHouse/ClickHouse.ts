import clickHouse from 'clickhouse'
export default class ClickHouseSingleton {
  private static _instance: ClickHouseSingleton
  private _clickhouse: any // todo: add type for 
  private constructor (clickhouseInstance: any) {
    this._clickhouse = clickhouseInstance
  }

  public get clickhouse () {
    return this._clickhouse
  }

  public static async getInstance() {
    if (!ClickHouseSingleton._instance) {
      console.log('создаем синглтон')
      const clickhouseInstance = new clickHouse.ClickHouse({ url: 'http://clickhouse' })

      // создаем тут таблмицу
      await clickhouseInstance.query(`
         CREATE TABLE IF NOT EXISTS testing 
         (data String) 
         ENGINE = Memory`).toPromise()
      console.log('Создали таблицу') 
      /*
        комментарий, в котором можно посмотреть работающий пример для раюоты с ch db
        await clickhouseInstance.query(`INSERT INTO testing VALUES ('hello world')`).toPromise()
        console.log('Добавили запись')
        const test = await clickhouseInstance.query(`SELECT data FROM testing`).toPromise()
        console.log('Gjkexbkb pfgbcm, test = ', test) 
      */
      ClickHouseSingleton._instance = new ClickHouseSingleton(clickhouseInstance) 
    }
    return ClickHouseSingleton._instance 
  }
}