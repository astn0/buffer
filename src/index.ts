import cors from 'cors'
import bodyParser from 'body-parser'
import routes from './routes/Routes'
import errors from './controllers/Errors'
import express from 'express'
import { Request, Response, NextFunction } from 'express'
import dotenv from 'dotenv'
import config from './configs/Config'
import LoadInterval from './libs/LoadInterval/LoadInterval'

dotenv.config()
config.port = process.env.PORT as string
config.bufferMax = process.env.bufferMax as string
config.timeToWait = process.env.timeToWait as string

const app: express.Application = express()

app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))
app.use(cors())

app.use('/api', routes)

app.use((request: Request, response: Response, next: NextFunction) => {
  next(new Error('404'))
});
app.use(errors.ERROR);

app.listen(config.port, function () {
  console.log(`App is listening on port ${config.port}!`)
});


const loadInterval = new LoadInterval(parseInt(config.bufferMax))
setInterval(() => {
  loadInterval.pushFromBufferToCH()
}, parseInt(config.timeToWait));
