import config from '../configs/Config'
import { Request, Response, NextFunction } from 'express'
import { check, validationResult } from 'express-validator'
import ClickHouseDataDomain from '../domains/ClickHouseDataDomain'
export default class DataController {
  /**
   * Контроллер для тестирования
   * @param req 
   * @param res 
   */
  public static async getData (req: Request, res: Response,): Promise<void> { 
    const clickHouseDataDomain = new ClickHouseDataDomain(parseInt(config.bufferMax))
    const result: Array<object> = await clickHouseDataDomain.getData().catch( err => {
      console.log('err = ', err)
      throw new Error(err)
    })
    res.json({
      result
    })
  }
  /**
   * Добавление данных
   * @param req 
   * @param res 
   */
  public static async addDataToBuffer (req: Request, res: Response,): Promise<void> {
    await check('chTableName').isString().run(req)
    await check('data').notEmpty().run(req)
    validationResult(req).throw()

    const chTableName: string = req.body.chTableName
    const data: object = req.body.data

    
    try {
      const clickHouseDataDomain = new ClickHouseDataDomain(parseInt(config.bufferMax))
      await clickHouseDataDomain.addData(chTableName, data)
    } catch(err) {
      console.log('err = ', err)
      throw new Error(err)
    }

    res.json({
      status: 'ok'
    })
  }
}