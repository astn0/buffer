import TestBufferModel from '../models/TestBufferModel'
import TestModel from '../models/TestModel'
export default class ClickHouseProxy {
  private readonly _bufferMax: number
  
  constructor (bufferMax: number) {
    this._bufferMax = bufferMax
  }
  /**
   * Забираем давнные из тестовой таблицы в бд
   */
  public  async getData (): Promise<Array<object>> {
    const data: Array<any> = await TestModel.getData()
    console.log(data)
    return data.map(x => JSON.parse(x.data))
  }
  /**
   * Забираем данные из буфера
   * @param tableName 
   */
  public async getDataFromBuffer (tableName: string): Promise<any> {
    let data: any = null
    if (tableName === 'test') {
      data = await TestBufferModel.getData()
    } else {
      throw new Error('Нет такой таблицы')
    }
    return data
  }
  /**
   * Добавление данных в буфер или в бд
   * 
   * @param tableName 
   * @param data 
   */
  public  async addData (tableName: string, data: object) {
    // todo: check buffer size
    const memory = await TestBufferModel.getMemory()
    if (memory > this._bufferMax) {
      console.log('пора загружать данные из redis в ch')
      
      this.pushFromBufferToCH(tableName)
      
      
    } 
    
    await this._addDataToBuffer(tableName, data) 
    
    
    
  }

  /**
   * Добавление данных d ch
   * 
   * @param tableName 
   * @param data 
   */
  public async addDataToCh (tableName: string, data: object) {
    
    if (tableName === 'test') {
      await TestModel.insertData(JSON.stringify(data))
    } else {
      throw new Error('Нет такой таблицы')
    }
  }
  /**
   * Вынимаем данные из буфера и кладем в ch
   * @param tableName 
   */
  public async pushFromBufferToCH(tableName: string): Promise<void> {
    console.log('Отправляем данные из хранилища в CH')
    const clickHouseProxy = new ClickHouseProxy(this._bufferMax)
    for (let index = 0; index !== 100; index++) {
      const dataFromBuffer = await clickHouseProxy.getDataFromBuffer(tableName)
      if (!dataFromBuffer) {
        console.log('буффер пуст')
        break
      }
      await clickHouseProxy.addDataToCh(tableName, JSON.parse(dataFromBuffer))
    }
  }

  /**
   * Добавление данных в буфер
   * 
   * @param tableName 
   * @param data 
   */
  private async _addDataToBuffer (tableName: string, data: object) {
    
    if (tableName === 'test') {
      await TestBufferModel.pushData(JSON.stringify(data))
    } else {
      throw new Error('Нет такой таблицы')
    }
  }
}