// import Redis from '../libs/Redis/Redis'
import Redis from 'ioredis'
export default class TestBufferModel {
  /**
   * Добавляем данные в редис
   * @param data 
   */
  public static async pushData (data: string): Promise<void> {
    const redis = new Redis(6379, 'redis')
    await redis.rpush('test', data)
    await redis.disconnect()
  }
  /**
   * Получаем данные из редиса
   */
  public static async getData (): Promise<any> { 
    const redis = new Redis(6379, 'redis')
    const result = await redis.rpop('test') 
    await redis.disconnect()
    return result
  }
  /**
   * Получаем общее кол-во используемой памяти из редиса
   */
  public static async getMemory (): Promise<number> { 
    const redis = new Redis(6379, 'redis')
    const info: string = await redis.info()
    await redis.disconnect()
    const memory: string = info.split('used_memory:')[1].split('\n')[0]
    return parseInt(memory)
  }
}