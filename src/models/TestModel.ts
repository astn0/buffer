import ClickHouseSingleton from '../libs/ClickHouse/ClickHouse'
/**
 * Класс для работы с БД Click house
 */
export default class TestModel {
  /**
   * Добавляем запись в таблицу test
   */
  public static async insertData (data: string): Promise<void> {
    const chInstance: ClickHouseSingleton = await ClickHouseSingleton.getInstance()
    await chInstance.clickhouse.query(`INSERT INTO testing VALUES ('${data}')`).toPromise()
  }
  /**
   * Читаем из таблицы test
   */
  public static async getData (): Promise<Array<any>> {
    const chInstance: ClickHouseSingleton = await ClickHouseSingleton.getInstance()
    const result: Array<any> = await chInstance.clickhouse.query(`SELECT data FROM testing`).toPromise()
    return result
  }

}