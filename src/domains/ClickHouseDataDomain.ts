import ClickHouseProxy from "../proxy/ClickHouseProxy"

export default class ClickHouseDataDomain {
  private readonly _bufferMax: number
  
  constructor (bufferMax: number) {
    this._bufferMax = bufferMax
  }
  /**
   * Получаем данные из таблицы ch (для тестирования только, в задаче этогй фичи нет)
   */
  public async getData (): Promise<Array<object>> {
    const clickHouseProxy = new ClickHouseProxy(this._bufferMax)
    const data: Array<object> = await clickHouseProxy.getData()
    return data
  }

  /**
   * Добавляем json объект в буфер 
   * 
   * @param chTableName 
   * @param data 
   */
  public async addData (chTableName: string, data: object): Promise<void> {
    const clickHouseProxy = new ClickHouseProxy(this._bufferMax)
    await clickHouseProxy.addData(chTableName, data)
  }
  
  /**
   * Отправляем данные из хранилища в CH
   */

  public async pushFromBufferToCH(): Promise<void> {
    const clickHouseProxy = new ClickHouseProxy(this._bufferMax)
    await clickHouseProxy.pushFromBufferToCH('test') // todo: Тут нужен цикл по всем таблицам
  }
}